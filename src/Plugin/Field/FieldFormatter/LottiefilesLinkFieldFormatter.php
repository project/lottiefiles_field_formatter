<?php

namespace Drupal\lottiefiles_field_formatter\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'lottiefiles_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "lottiefiles_link",
 *   label = @Translation("Lottiefiles Link Field Formatter"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LottiefilesLinkFieldFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'autoplay' => TRUE,
      'background' => 'transparent',
      'controls' => FALSE,
      'direction' => 1,
      'hover' => FALSE,
      'loop' => FALSE,
      'mode' => 'PlayMode.Normal',
      'renderer' => 'svg',
      'speed' => 1,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['autoplay'] = [
      '#title' => $this->t('Autoplay'),
      '#type' => 'checkbox',
      '#description' => $this->t('When set to true, automatically plays the animation on loading it'),
      '#default_value' => $this->getSetting('autoplay'),
    ];
    $element['background'] = [
      '#title' => $this->t('Background Color'),
      '#type' => 'textfield',
      '#size' => 10,
      '#description' => $this->t("Enter color code in the format '#FFFFFF'. By default, the background is 'transparent' and will take the color of the parent container."),
      '#default_value' => Xss::filter($this->getSetting('background')),
      '#required' => TRUE,
    ];
    $element['controls'] = [
      '#title' => $this->t('Controls'),
      '#type' => 'checkbox',
      '#description' => $this->t('Display animation controls: Play, Pause & Slider'),
      '#default_value' => $this->getSetting('controls'),
    ];
    $elements['count'] = [
      '#type' => 'number',
      '#title' => t('Count'),
      '#description' => $this->t('Number of times to loop animation. Set 0 to set undefined.'),
      '#min' => 0,
      '#default_value' => $this->getSetting('count'),
      '#required' => TRUE,
    ];
    $element['direction'] = [
      '#title' => $this->t('Direction'),
      '#type' => 'select',
      '#description' => $this->t('Direction of the animation. Set to 1 to play the animation forward or set to -1 to play it backward.'),
      '#default_value' => $this->getSetting('direction'),
      '#options' => [
        '1' => $this->t('Forward'),
        '-1' => $this->t('Backward'),
      ],
    ];
    $element['hover'] = [
      '#title' => $this->t('Hover'),
      '#type' => 'checkbox',
      '#description' => $this->t('When set to true, plays animation on mouse over.'),
      '#default_value' => $this->getSetting('hover'),
    ];
    $element['loop'] = [
      '#title' => $this->t('Loop'),
      '#type' => 'checkbox',
      '#description' => $this->t('When set to true, loops the animation.'),
      '#default_value' => $this->getSetting('loop'),
    ];
    $element['mode'] = [
      '#title' => $this->t('Mode'),
      '#type' => 'select',
      '#description' => $this->t('Direction of the animation. Set to 1 to play the animation forward or set to -1 to play it backward.'),
      '#options' => [
        'PlayMode.Normal' => $this->t('Normal'),
        'PlayMode.Bounce' => $this->t('Bounce'),
      ],
      '#default_value' => $this->getSetting('mode'),
    ];
    $element['renderer'] = [
      '#title' => $this->t('Renderer'),
      '#type' => 'select',
      '#description' => $this->t('The renderer to use for the animation.'),
      '#options' => [
        'svg' => $this->t('SVG'),
        'canvas' => $this->t('Canvas'),
      ],
      '#default_value' => $this->getSetting('renderer'),
    ];
    $element['speed'] = [
      '#title' => $this->t('Animation speed.'),
      '#type' => 'number',
      '#description' => $this->t('Set this parameter to any positive number.'),
      '#default_value' => $this->getSetting('speed'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Autoplay: %autoplay', ['%autoplay' => $this->getSetting('autoplay') ? $this->t('Yes') : $this->t('No')]);
    $summary[] = $this->t('Background:') . $this->getSetting('background');
    $summary[] = $this->t('Controls: %controls', ['%controls' => $this->getSetting('controls') ? $this->t('Yes') : $this->t('No')]);
    $summary[] = $this->t('Count:') . $this->getSetting('count');
    $summary[] = $this->t('Direction:') . $this->getSetting('direction');
    $summary[] = $this->t('Hover: %hover', ['%hover' => $this->getSetting('hover') ? $this->t('Yes') : $this->t('No')]);
    $summary[] = $this->t('Loop: %loop', ['%loop' => $this->getSetting('loop') ? $this->t('Yes') : $this->t('No')]);
    $summary[] = $this->t('Mode:') . $this->getSetting('mode');
    $summary[] = $this->t('Renderer:') . $this->getSetting('renderer');
    $summary[] = $this->t('Speed:') . $this->getSetting('speed');
    return $summary;

  }

  /**
   * Format the settings as attributes.
   */
  private function getAttributes() {
    $attributes = new Attribute();
    if ($this->getSetting('autoplay')) {
      $attributes->setAttribute('autoplay', $this->getSetting('autoplay'));
    }
    $attributes->setAttribute('background', $this->getSetting('background'));
    if ($this->getSetting('controls')) {
      $attributes->setAttribute('controls', $this->getSetting('controls'));
    }
    if ($this->getSetting('count') > 0) {
      $attributes->setAttribute('count', $this->getSetting('count'));
    }
    $attributes->setAttribute('direction', $this->getSetting('direction'));
    if ($this->getSetting('hover')) {
      $attributes->setAttribute('hover', $this->getSetting('hover'));
    }
    if ($this->getSetting('loop')) {
      $attributes->setAttribute('loop', $this->getSetting('loop'));
    }
    $attributes->setAttribute('mode', $this->getSetting('mode'));
    $attributes->setAttribute('renderer', $this->getSetting('renderer'));
    if ($this->getSetting('speed') > 1) {
      $attributes->setAttribute('speed', $this->getSetting('speed'));
    }

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $attributes = $this->getAttributes();
    foreach ($items as $delta => $item) {
      $url = $this->buildUrl($item);
      $attributes->setAttribute('src', $url->toString());
      $elements[$delta] = [
        '#theme' => 'lottiefiles_field_formatter',
        '#attributes' => $attributes,
      ];
    }

    return $elements;

  }

}
