# Lottiefiles Field Formatter
## Introduction
This module simplifies the integration of Lottiefiles animation into your Drupal website by providing field formatter for File and Link type field. With this module, you can seamlessly render uploaded Lottie JSON files or links to LottieJSON files as lottie animations, enhancing your website with dynamic and engaging visual content.
## Installation
[Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
## Usage
1. For File field: configure allowed file type as .json. Under manage display tab choose format as "Lottiefiles File Field Formatter". 
2. For Link field: Under manage display tab choose format as "Lottiefiles Link Field Formatter". 
